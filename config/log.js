/**
 * Built-in Log Configuration
 * (sails.config.log)
 *
 * Configure the log level for your app, as well as the transport
 * (Underneath the covers, Sails uses Winston for logging, which
 * allows for some pretty neat custom transports/adapters for log messages)
 *
 * For more information on the Sails logger, check out:
 * http://sailsjs.org/#!/documentation/concepts/Logging
 */

var winston = require('winston');
var DailyRotateFile = require('winston-daily-rotate-file');
var logFile = './logs/queueloadapp.log';

var fileRotateOptions =
{
  filename: logFile
};

winston.add(DailyRotateFile, fileRotateOptions);

var logger = new (winston.Logger)(
  {
    transports:
    [
      //This transport write to the console. Log format# <timestamp> - <level>: <level>: <message>
      // e.g. 2016-03-17T13:56:23.485Z - debug: debug: Environment : development
      new (winston.transports.Console)(
        {
          level: 'silly',
          timestamp: true,
          colorize: true,
          json: false
        }),
      //This transport writes to the file. Log format# <timestamp> - <level>: <level>: <message>
      // e.g. 2016-03-17T13:56:23.485Z - debug: debug: Environment : development
      new (DailyRotateFile)(
        {
          datePattern: 'yyyy-MM-dd',
          filename: logFile,
          level: 'debug',
          timestamp: true,
          colorize: false,
          json: false
        }),
    ]
});


module.exports.log = {

  /***************************************************************************
  *                                                                          *
  * Valid `level` configs: i.e. the minimum log level to capture with        *
  * sails.log.*()                                                            *
  *                                                                          *
  * The order of precedence for log levels from lowest to highest is:        *
  * silly, verbose, info, debug, warn, error                                 *
  *                                                                          *
  * You may also set the level to "silent" to suppress all logs.             *
  *                                                                          *
  ***************************************************************************/

  /***************************************************************************
  * Winston logging levels:                                                  *
  *                                                                          *
  * The order of precedence for log levels from lowest to highest is:        *
  * silly, debug, verbose, info, warn, error                                 *
  *                                                                          *
  *                                                                          *
  ***************************************************************************/

  prefixes: {},

  //level: 'info'

  //level: 'silly',
  //timestamp: true,
  //colorize: true,
  //json: false,
  custom: logger,
  level: 'debug',

  colors: {
    silly: 'rainbow',
    input: 'grey',
    verbose: 'cyan',
    prompt: 'grey',
    info: 'green',
    data: 'grey',
    help: 'cyan',
    warn: 'yellow',
    debug: 'blue',
    error: 'red'
  }
};

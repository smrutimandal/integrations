/* FileName     : Gentuils.js
 *
 * @description  : Generic utility functions.
 *                Unless specified all the functions return a promise object.
 *
 *   Date              Change Description                            Author
 * ---------         ----------------------                        ---------
 * 14/04/2017         Initial file creation                         SMandal
 *
 */
var request = require('request-promise');

 module.exports =
 {
 	randgen: function(length, type)
	{
	  var text = "";
	  var possible;
	  // 1 = Alphabetic
	  // 2 = Numeric
	  // 3 = Alphanumeric
	  switch(type)
	  {
	  	case 1: 
	  		possible = "abcdefghijklmnopqrstuzwxyz";
	  		break;
	  	case 2: 
	  		possible = "0123456789";
	  		break;
	  	case 3: 
	  		possible = "abcdefghijklmnopqrstuzwxyz0123456789";
	  		break;
	  	default: 
	  		return;
	  }	
	  for( var i=0; i < length; i++ )
	  {
	    text += possible.charAt(Math.floor(Math.random() * possible.length));
	 	}
	 	return text;
	},
	getBotUpdates: function(token)
	{
		var options = 
    {
      method: 'GET',
      uri: "https://api.telegram.org/bot" + token + "/getUpdates",
      json: true
    };
    return request(options);
	}
};
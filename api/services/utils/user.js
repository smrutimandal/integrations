/* FileName     : User.js
 *
 * @description  : All the commonly used functions for the User.
 *                Unless specified all the functions return a promise object.
 *
 *   Date              Change Description                            Author
 * ---------         ----------------------                        ---------
 * 17/12/2015         Initial file creation                         SMandal
 * 02/03/2016         Added comments, getUserByState                bones
 *
 */

 var Promise = require("bluebird");

 module.exports =
 {
/**
  * Name: getUser
  * @description: This function fetches all information about an user, including:
  * his associations with queues if any, owned CBs if any, administered BDs if any.
  *
  * @param:  array of User Names
  * @returns: Promise Object
  */
  getUser: function(userNames)
  {
    sails.config.connections.localDataRepo.database = 'j8ohaqqy';
    sails.hooks.orm.reload();
    console.log("Model - after", sails.config.connections);
    var fn = function asyncLookup(name)
    {    
      return  User.findOne({ username: name })
                  .populate("administeredbds")
                  .populate("ownedcbs")
                  .populate("ownedqueues")
                  .populate("queuesenrolled")
                  .populate("incmgrqueues")
                  .populate("servicestateapprovalids")
                  .populate("accountid")
                  .populate("roles");
    };
    var actions = userNames.map(fn);
    var pResults = Promise.all(actions);
    return pResults;
  }
 };

/* FileName: ValidationPatterns.js
* @description: This file contains all the custom validation patterns used in our models .
*
* Date                        Change Description                               Author
* ---------               ---------------------------                        -----------
* 05/02/2016                Initial file creation                              bones
*
*/

module.exports =
{
  alphaNumericDotDash: /(^[0-9A-Z])([-0-9A-Z.])+$/i,
  password: /^[0-9A-Z._!@#$%^&*()]+$/i, //TODO: Need to comply with One Num, One Upper, One Lower, One Special(!@#$%^&*)
  phoneNumber: /\d{10}/,
  countryCode: /\+[0-9]{1,3}$/,
  alphaNumericDotDashSpace: /(^[0-9A-Z])([-0-9A-Z. ])+$/i,
  alphaSpace: /(^[A-Z])([A-Z ])+$/i,
  macaddress: /^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/i
};

/**
* FileName: isAzureAuthenticated.js
* @description :: Policy to verify authenticity of a request from Azure
*/

var errorCode = require("../resources/ErrorCodes.js");
var pgp = require('pg-promise')();
var cache = require('memory-cache');
var enums = require('../resources/Enums.js');

module.exports = function(req, res, next) 
{
	if(!req.headers['content-type'] || !req.headers['content-type'].includes('application/json') || !req.query.qlaccountid || !req.query.azurekey)
	{
		var error = new Error();
		error.code = 400051;
		error.message = errorCode['400051'];
		next(error);
	}
	else
	{
		var azurekey = cache.get(req.query.qlaccountid + '-azurekey');
		if(azurekey === req.query.azurekey)
		{
			next();
		}
		else
		{
    	var db = cache.get(req.query.qlaccountid);
    	if(!db)
    	{
    		var connection = 
    		{
					database: req.query.qlaccountid,
					user: req.query.qlaccountid,
					password: req.query.qlaccountid
				};
				if(sails.config.environment === 'development' || sails.config.environment === 'staging' || sails.config.environment === 'test')
				{
				  connection.host = sails.config.connections.localDataRepo.host;
				  connection.port = sails.config.connections.localDataRepo.port;
				}
				else if(sails.config.environment === 'production')
				{
				  connection.host = sails.config.connections.awsDataRepo.host;
				  connection.port = sails.config.connections.awsDataRepo.port;
				}
				else
				{
				  next({message: "No environment set!!"});
				}
    	  db = pgp(connection);
    	  cache.put(req.query.qlaccountid, db);
    	}
    	var getAzureKey = db.any(
    	  {
    	    text: "select config->'azurekey' as key from integrationconfigs where integrationtype=$1;", 
    	    values: [enums.integrationService.AZURE]
    	  }
    	);
    	getAzureKey
    	.then(function (result)
    	{
    		sails.log.info("result", result);
    		if(!result.length || !result[0].key || (result[0].key !== req.query.azurekey))
    		{
    			var error = new Error();
					error.code = 401;
					error.message = "Incorrect verification code";
					next(error);
    		}
    		else
    		{
    			cache.put(req.query.qlaccountid + '-azurekey', req.query.azurekey);
    			next();
    		}
    	})
    	.catch(function (error)
    	{
    		next(error);
    	});
		}
	}  
};
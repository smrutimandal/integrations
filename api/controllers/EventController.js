/* FileName: EventController.js
* @description: This file describes the various actions in the events controller.
*
* Date          Change Description                            Author
* ---------     ----------------------                        -------
* 22/05/2016    Initial file creation                         SMandal
*
*/

var errorCode = require("../resources/ErrorCodes.js");
var promise = require('bluebird');
var _ = require('lodash');
var enums = require("../resources/Enums.js");
var RedisMQ = require("rsmq");
var rsmqOptions = { ns: "rsmq" };
var connection = {
  user: 'qlodtest',
  database: 'qlodtest',
  password: 'qlodtest',
  port: 5432
};

if(sails.config.environment === 'development' || sails.config.environment === 'staging' || sails.config.environment === 'test')
{
  rsmqOptions.host = sails.config.connections.localRefTknStore.host;
  rsmqOptions.port = sails.config.connections.localRefTknStore.port;
  connection.host = sails.config.connections.localAccountsRepo.host;
  connection.port = sails.config.connections.localAccountsRepo.port;
}
else if(sails.config.environment === 'production')
{
  rsmqOptions.host = sails.config.connections.awsRefTknStore.host;
  rsmqOptions.port = sails.config.connections.awsRefTknStore.port;
  connection.host = sails.config.connections.awsDataRepo.host;
  connection.port = sails.config.connections.awsDataRepo.port;
}
else
{
  sails.log.error("No environment set!!");
}
var pgp = require('pg-promise')();
var cache = require('memory-cache');

var rsmq = new RedisMQ(rsmqOptions);
promise.promisifyAll(rsmq);

var AWS = require('aws-sdk');
AWS.config.setPromisesDependency(require('bluebird'));

module.exports = 
{
  receivecwevents: function(req, res)
  {
    if(!req.query.qlaccountid || !req.query.awscwkey)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode['400051'];
      return res.negotiate(error);
    }
    else
    {
      if(req.body.Type === "SubscriptionConfirmation")
      {
        var db;
        var params = {
          Token: req.body.Token,
          TopicArn: req.body.TopicArn
        };
        var splitParam = _.split(req.body.TopicArn, ':');
        var sns = new AWS.SNS({ accessKeyId: sails.config.awscfg.accessKeyId, secretAccessKey: sails.config.awscfg.secretAccessKey, region: splitParam[3] });
        
        sns.confirmSubscription(params).promise()
        .then(function (result) 
        {
          connection.database = req.query.qlaccountid;
          connection.user = req.query.qlaccountid;
          connection.password = req.query.qlaccountid;
          db = cache.get(connection.database);
          if(!db)
          {
            db = pgp(connection);
            cache.put(connection.database, db);
          }
          return db.any(
          {
            text: "SELECT config from integrationconfigs where integrationtype=$1;",
            values: [ enums.integrationService.CLOUDWATCH ]
          });           
        })
        .then(function (result)
        {
          result[0].config.confirmed = true;
          return db.any(
          {
            text: "UPDATE integrationconfigs SET config=$1 where integrationtype=$2",
            values: [ result[0].config, enums.integrationService.CLOUDWATCH ]
          });          
        })
        .then(function (result)
        {
          return res.ok();
        })
        .catch(function (error)
        {
          return res.negotiate(error);
        });
      }
      else
      {
        var msgqname = "awscw-events";
        var dlqname = "awscw-events-deadletter";
        rsmq.listQueuesAsync()
        .then(function (queues)
        {
          if(!queues.length || !_.includes(queues, msgqname) || !_.includes(queues, dlqname))
          {
            var pCreations = [];
            if(queues.indexOf(msgqname) < 0)
            {
              pCreations.push(rsmq.createQueueAsync({qname: msgqname, vt: 60, maxsize: -1}));
            }
            if(queues.indexOf(dlqname) < 0)
            {
              pCreations.push(rsmq.createQueueAsync({qname: dlqname, vt: 60, maxsize: -1}));
            }
            return Promise.all(pCreations);
           }
           else
           {
             return queues;
           }
        })    
        .then(function ()
        {
          var jsonMessage = JSON.parse(req.body.Message);
          jsonMessage.qlaccountid = req.query.qlaccountid;
          var data = {body: {}};
          if(JSON.stringify(req.body).length <= 131072)
          {
            data.ispresent = true;
            data.body = JSON.stringify(req.body);
          }
          else
          {
            data.ispresent = false;
          }
          jsonMessage.rawdata = data;
          return rsmq.sendMessageAsync({qname: msgqname, message: JSON.stringify(jsonMessage)});
        })
        .then(function ()
        {
          return res.ok();
        })
        .catch(function (error)
        {
          return res.negotiate(error);
        });
      }
    }
  },
  receivesentryevents: function(req, res)
  {
    if(!req.query.qlaccountid || !req.query.sentrykey)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode['400051'];
      return res.negotiate(error);
    }
    var msgqname = "sentry-events";
    var dlqname = "sentry-events-deadletter";
    rsmq.listQueuesAsync()
    .then(function (queues)
    {
      if(!queues.length || !_.includes(queues, msgqname) || !_.includes(queues, dlqname))
      {
        var pCreations = [];
        if(queues.indexOf(msgqname) < 0)
        {
          pCreations.push(rsmq.createQueueAsync({qname: msgqname, vt: 60, maxsize: -1}));
        }
        if(queues.indexOf(dlqname) < 0)
        {
          pCreations.push(rsmq.createQueueAsync({qname: dlqname, vt: 60, maxsize: -1}));
        }
        return Promise.all(pCreations);
      }
      else
      {
        return queues;
      }
    })
    .then(function ()
    {
      var jsonMessage = req.body;
      jsonMessage.qlaccountid = req.query.qlaccountid;
      var data = {body: {}};
      if(JSON.stringify(req.body).length <= 131072)
      {
        data.ispresent = true;
        data.body = JSON.stringify(req.body);
      }
      else
      {
        data.ispresent = false;
      }
      jsonMessage.rawdata = data;
      return rsmq.sendMessageAsync({qname: msgqname, message: JSON.stringify(jsonMessage)});
    })
    .then(function ()
    {
      return res.json();
    })
    .catch(function (error)
    {
      return res.negotiate(error);
    });
  },
  receivenagiosevents: function(req, res)
  {
    var msgqname = "nagios-events";
    var dlqname = "nagios-events-deadletter";
    rsmq.listQueuesAsync()
    .then(function (queues)
    {
      if(!queues.length || !_.includes(queues, msgqname) || !_.includes(queues, dlqname))
      {
        var pCreations = [];
        if(queues.indexOf(msgqname) < 0)
        {
          pCreations.push(rsmq.createQueueAsync({qname: msgqname, vt: 60, maxsize: -1}));
        }
        if(queues.indexOf(dlqname) < 0)
        {
          pCreations.push(rsmq.createQueueAsync({qname: dlqname, vt: 60, maxsize: -1}));
        }
        return Promise.all(pCreations);
      }
      else
      {
        return queues;
      }
    })
    .then(function ()
    {
      var jsonMessage = req.body;
      jsonMessage.qlaccountid = req.query.qlaccountid;
      var data = {body: {}};
      if(JSON.stringify(req.body).length <= 131072)
      {
        data.ispresent = true;
        data.body = JSON.stringify(req.body);
      }
      else
      {
        data.ispresent = false;
      }
      jsonMessage.rawdata = data;
      return rsmq.sendMessageAsync({qname: msgqname, message: JSON.stringify(jsonMessage)});
    })
    .then(function ()
    {
      return res.json();
    })
    .catch(function (error)
    {
      return res.negotiate(error);
    });
  },
  receivelogglyevents: function(req, res)
  {
    var msgqname = "loggly-events";
    var dlqname = "loggly-events-deadletter";
    rsmq.listQueuesAsync()
    .then(function (queues)
    {
      if(!queues.length || !_.includes(queues, msgqname) || !_.includes(queues, dlqname))
      {
        var pCreations = [];
        if(queues.indexOf(msgqname) < 0)
        {
          pCreations.push(rsmq.createQueueAsync({qname: msgqname, vt: 60, maxsize: -1}));
        }
        if(queues.indexOf(dlqname) < 0)
        {
          pCreations.push(rsmq.createQueueAsync({qname: dlqname, vt: 60, maxsize: -1}));
        }
        return Promise.all(pCreations);
      }
      else
      {
        return queues;
      }
    })
    .then(function ()
    {
      var jsonMessage = req.body;
      var strHits = JSON.stringify(jsonMessage.recent_hits);
      if(strHits.length > 8192)
      {
        strHits = strHits.slice(0, 8191);
      }
      jsonMessage.recent_hits = strHits;
      jsonMessage.qlaccountid = req.query.qlaccountid;
      var data = {body: {}};
      if(JSON.stringify(req.body).length <= 131072)
      {
        data.ispresent = true;
        data.body = JSON.stringify(req.body);
      }
      else
      {
        data.ispresent = false;
      }
      jsonMessage.rawdata = data;
      return rsmq.sendMessageAsync({qname: msgqname, message: JSON.stringify(jsonMessage)});
    })
    .then(function ()
    {
      return res.json();
    })
    .catch(function (error)
    {
      return res.negotiate(error);
    });
  },
  receivecustomevents: function(req, res)
  {
    var msgqname = "custom-events";
    var dlqname = "custom-events-deadletter";
    if(!req.body.reportedAt || req.body.reportedAt < 0 || req.body.reportedAt > Math.floor(Date.now()/1000) || 
      req.body.region || !req.body.type || req.body.type < 1 || req.body.type > enums.alarmType.LIMIT ||
      !req.body.name || !req.body.summary)
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode [ '400051'];
      return res.negotiate(error);
    }
    rsmq.listQueuesAsync()
    .then(function (queues)
    {
      if(!queues.length || !_.includes(queues, msgqname) || !_.includes(queues, dlqname))
      {
        var pCreations = [];
        if(queues.indexOf(msgqname) < 0)
        {
          pCreations.push(rsmq.createQueueAsync({qname: msgqname, vt: 60, maxsize: -1}));
        }
        if(queues.indexOf(dlqname) < 0)
        {
          pCreations.push(rsmq.createQueueAsync({qname: dlqname, vt: 60, maxsize: -1}));
        }
        return Promise.all(pCreations);
      }
      else
      {
        return queues;
      }
    })
    .then(function ()
    {
      var jsonMessage = req.body;
      var time = jsonMessage.reportedAt;
      delete jsonMessage.reportedAt;
      jsonMessage.reportedAt = time;
      jsonMessage.qlaccountid = req.query.qlaccountid;
      var data = {body: {}};
      if(JSON.stringify(req.body).length <= 131072)
      {
        data.ispresent = true;
        data.body = JSON.stringify(req.body);
      }
      else
      {
        data.ispresent = false;
      }
      jsonMessage.rawdata = data;
      return rsmq.sendMessageAsync({qname: msgqname, message: JSON.stringify(jsonMessage)});
    })
    .then(function ()
    {
      return res.json();
    })
    .catch(function (error)
    {
      return res.negotiate(error);
    });
  },
  receiveazureevents: function(req, res)
  {
    sails.log.info("[EventController.receiveazureevents] Payload", req.body);
    return res.json(req.body);
  }
};
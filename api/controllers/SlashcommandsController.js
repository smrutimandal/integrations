/* FileName: SlashCommandsController.js
*  @description: Slash commands module
*
*    Date            Change Description                            Author
*  ---------     ---------------------------                     ----------
*  30/03/2016        Initial file creation                         SMandal
*
*/

var errorCode = require("../resources/ErrorCodes.js");
var enums = require("../resources/Enums.js");
var pgp = require('pg-promise')();
var cache = require('memory-cache');

var connection = {
  user: 'qlodtest',
  database: 'qlodtest',
  password: 'qlodtest'
};

if(sails.config.environment === 'development' || sails.config.environment === 'staging' || sails.config.environment === 'test')
{
  connection.host = sails.config.connections.localAccountsRepo.host;
  connection.port = sails.config.connections.localAccountsRepo.port;
}
else if(sails.config.environment === 'production')
{
  connection.host = sails.config.connections.awsAccountsRepo.host;
  connection.port = sails.config.connections.awsDataRepo.port;
}
else
{
  sails.log.error("No environment set!!");
}

module.exports = {
  getuser: function(req, res)
  {
    if(!req.body.text || Array.isArray(req.body.text))
    {
      var error = new Error();
      error.code = 400051;
      return res.negotiate(error);
    }
    var db;
    var output = {};
    Account.findOne({ select: [ 'database' ], where: { slackteamid: req.body.team_id } })
    .then(function (result)
    {
      connection.database = result.database;
      connection.user = result.database;
      connection.password = result.database;
      db = cache.get(connection.database);
      if(!db)
      {
        db = pgp(connection);
        cache.put(connection.database, db);
      }
      return db.any(
      {
        text: "select users.username, users.fullname, users.emailid, users.countrycode, users.phonenumber, users.isactive,incidents.incidentid from users \
                left outer join chargebackcodes on chargebackcodes.cbownerid = users.userid \
                left outer join incidents on (incidents.assigneduserid = users.userid AND incidents.visibility = 'operator') OR (incidents.owneruserid = users.userid AND incidents.visibility = 'owner') \
                where users.username = $1 and incidents.state != $2",
        values: [req.body.text, enums.incidentStates.CLOSED]
      });
    })
    .then(function (details)
    {
      if(!details.length)
      {
        var error = new Error();
        error.code = 400544;
        throw error;
      }
      else
      {
        var padding = function (number, width, paddingchar) 
        {
          paddingchar = paddingchar || '0';
          number = number + '';
          return number.length >= width ? number : new Array(width - number.length + 1).join(paddingchar) + number;
        };
        var pickIncs = function(objects)
        {
          var inclist = '';
          for(var i = 0; i < objects.length; i++)
          {
            if(objects[i].incidentid)
            {
              inclist = inclist + "INC" + padding(objects[i].incidentid, 5, 0) + '\n';
            }
          }
          return inclist;
        };
        var output =
        {
          "attachments": 
          [
            {
              "fallback": "[User Summary] " + req.body.text,
              "color": "#663399",
              "pretext": "User details: " + req.body.text,
              "title": details[0].username,
              "title_link": "https://app.queueload.com/",
              "fields": 
              [
                {
                  "title": "Full Name",
                  "value": details[0].fullname,
                  "short": true
                },
                {
                  "title": "Email ID",
                  "value": details[0].emailid,
                  "short": true
                },
                {
                  "title": "Country Code",
                  "value": details[0].countrycode || "Nill",
                  "short": true
                },
                {
                  "title": "Phone Number",
                  "value": (details[0].countrycode) ? details[0].phonenumber : "Nill",
                  "short": true
                },
                {
                  "title": "Status",
                  "value": (details[0].isactive) ? "Active" : "Not active",
                  "short": true
                },
                {
                  "title": "List of active incidents",
                  "value": pickIncs(details) || "Nill",
                  "short": true
                }
              ]
            }
          ]
        };
        return res.ok(output);
      }
    })
    .catch(function (error)
    {
      if(error.code === 400544)
      {
        output = 
        {
          "attachments": 
          [
            {
              "fallback": "Requested User " + req.body.text + " doesn't exist",
              "title": "Requested User " + req.body.text + " doesn't exist",
              "title_link": "https://app.queueload.com/",
              "text": "Please provide a valid user name",
              "color": "warning"
            }
          ]
        };
        return res.ok(output);
      }
      else
      {
        return res.negotiate(error);  
      }
    });
  },
  getinc: function(req, res)
  {
    if(!req.body.text || Array.isArray(req.body.text))
    {
      var error = new Error();
      error.code = 400051;
      return res.negotiate(error);
    }
    var incdetails;
    var db;
    var output = {};
    Account.findOne({ select: [ 'database' ], where: { slackteamid: req.body.team_id } })
    .then(function (result)
    {
      connection.database = result.database;
      connection.user = result.database;
      connection.password = result.database;
      db = cache.get(connection.database);
      if(!db)
      {
        db = pgp(connection);
        cache.put(connection.database, db);
      }
      var incident = req.body.text;
      return db.any(
      {
        text: "select incidents.incidentid,incidents.title,incidents.description,incidents.state,services.servicename,services.serviceid,incidents.severity,incidents.startedat,cidetails.ciname,cidetails.ipaddress,queues.queuename as assignedqueue,users.username as assigneduser from incidents join services on incidents.serviceid = services.serviceid left outer join cidetails on incidents.ciid = cidetails.ciid join queues on incidents.assignedqueueid = queues.queueid left outer join users on incidents.assigneduserid = users.userid where incidents.incidentid = $1", 
        values: [incident.replace( /^\D+/g, '')]
      });
    })
    .then(function (result)
    {
      incdetails = result;
      if(!incdetails.length)
      {
        var error = new Error();
        error.code = 400541;
        error.message = errorCode [ '400541' ];
        throw error;
      }
      else
      {
        return db.any(
        {
          text: "select incidentid from incidents where incidentid != $1 and serviceid = $2 and state != $3",
          values: [incdetails[0].incidentid, incdetails[0].serviceid, enums.incidentStates.CLOSED]
        });
      }
    })
    .then(function (result)
    { 
      var date = new Date(incdetails[0].startedat*1000);
      var dateofmonth = "0" + date.getDate();
      var month = "0" + (date.getMonth() + 1);
      var year = date.getFullYear();
      var hours = "0" + date.getHours();
      var minutes = "0" + date.getMinutes();
      var seconds = "0" + date.getSeconds();
      var inclist = '';
      var padding = function (number, width, paddingchar) 
      {
        paddingchar = paddingchar || '0';
        number = number + '';
        return number.length >= width ? number : new Array(width - number.length + 1).join(paddingchar) + number;
      };
      for(var iter = 0; iter < result.length; iter++)
      {
        inclist = inclist + "INC" + padding(result[iter].incidentid, 5, 0) + '\n';
      }
      var enumToString = function (enums, value) 
      {
        for (var k in enums)
        {
          if (enums[k] === value) 
            return k;
        }
        return null;
      };
      var incSev = enumToString(enums.incidentSeverity, incdetails[0].severity);
      var incState = enumToString(enums.incidentStates, incdetails[0].state);
      output = 
      {
        "attachments": 
        [
          {
            "fallback": "[Incident Summary] " + req.body.text,
            "color": "#663399",
            "pretext": "Incident details: " + req.body.text,
            "title": incdetails[0].title,
            "title_link": "https://app.queueload.com/",
            "text": incdetails[0].description,
            "fields": 
            [
              {
                "title": "Service Name",
                "value": incdetails[0].servicename,
                "short": true
              },
              {
                "title": "State",
                "value": incState,
                "short": true
              },
              {
                "title": "Severity",
                "value": incSev.charAt(0).toUpperCase() + incSev.substr(1).toLowerCase(),
                "short": true
              },
              {
                "title": "Start Time",
                "value": dateofmonth.substr(-2) + "/" + month.substr(-2) + "/" + year + " " + hours.substr(-2) + ":" + minutes.substr(-2) + ":" + seconds.substr(-2) + " GMT",
                "short": true
              },
              {
                "title": "Other active incidents of the service",
                "value": inclist || "No other active incidents",
                "short": true
              },
              {
                "title": "Configuration Item",
                "value": incdetails[0].ciname || "Not assigned",
                "short": true
              },
              {
                "title": "IP Address",
                "value": incdetails[0].ipaddress || "Nill",
                "short": true
              },
              {
                "title": "Assigned Queue",
                "value": incdetails[0].assignedqueue,
                "short": true
              },
              {
                "title": "Assigned User",
                "value": incdetails[0].assigneduser || "Not assigned",
                "short": true
              }
            ]
          }
        ]
      };
      return res.ok(output);
    })
    .catch(function (error)
    {
      if(error.code === 400541)
      {
        output = 
        {
          "attachments": 
          [
            {
              "fallback": "Requested incident ID " + req.body.text + " doesn't exist",
              "title": "Requested incident ID " + req.body.text + " doesn't exist",
              "title_link": "https://app.queueload.com/",
              "text": "Please provide a valid incident id",
              "color": "warning"
            }
          ]
        };
        return res.ok(output);
      }
      else
      {
        return res.negotiate(error);  
      }
    });
  },
  getservice: function(req, res)
  {
    if(!req.body.text || Array.isArray(req.body.text))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var db;
    var output = {};
    Account.findOne({ select: [ 'database' ], where: { slackteamid: req.body.team_id } })
    .then(function (result)
    {
      connection.database = result.database;
      connection.user = result.database;
      connection.password = result.database;
      db = cache.get(connection.database);
      if(!db)
      {
        db = pgp(connection);
        cache.put(connection.database, db);
      }
      var pGetSerivce = db.any(
      {
        text: "select services.servicename, services.details, services.servicestateid, services.isfrozen, services.freezecause, cidetails.ciname \
                from services \
                left outer join cidetail_serviceids__service_ciids \
                on service_ciids = services.serviceid \
                left outer join cidetails \
                on cidetail_serviceids__service_ciids.cidetail_serviceids = cidetails.ciid \
                where services.servicename = $1;",
        values: [req.body.text]
      });

      var pGetIncidents = db.any(
      {
        text: "select incidents.incidentid from incidents \
                left outer join services \
                on incidents.serviceid = services.serviceid \
                where services.servicename = $1 \
                and incidents.state != $2;",
        values: [req.body.text, enums.incidentStates.CLOSED]
      });
      return Promise.all([pGetSerivce, pGetIncidents]);
    })
    .then(function (details)
    {
      if(!details[0].length)
      {
        var error = new Error();
        error.code = 400542;
        throw error;
      }
      else
      {
        var servicedetails = details[0];
        var incdetails = details[1];
        var enumToString = function (enums, value) 
        {
          for (var k in enums)
          {
            if (enums[k] === value) 
              return k;
          }
          return null;
        };
        var serState = enumToString(enums.serviceStateIds, servicedetails[0].servicestateid);
        var freezeCause = enumToString(enums.freezeCauses, servicedetails[0].freezecause);
        var pickCis = function(objects)
        {
          var ciList = '';
          for(var i = 0; i < objects.length; i++)
          {
            ciList = ciList + objects[i].ciname + '\n';
          }
          return ciList;
        };

        var padding = function (number, width, paddingchar) 
        {
          paddingchar = paddingchar || '0';
          number = number + '';
          return number.length >= width ? number : new Array(width - number.length + 1).join(paddingchar) + number;
        };
        var pickIncs = function(objects)
        {
          var inclist = '';
          for(var i = 0; i < objects.length; i++)
          {
            inclist = inclist + "INC" + padding(objects[i].incidentid, 5, 0) + '\n';
          }
          return inclist;
        };        
        output = 
        {
          "attachments": 
          [
            {
              "fallback": "[Service Summary] " + req.body.text,
              "color": "#663399",
              "pretext": "Service details: " + req.body.text,
              "title": servicedetails[0].servicename,
              "title_link": "https://app.queueload.com/",
              "text": servicedetails[0].details,
              "fields": 
              [
                {
                  "title": "Service State",
                  "value": serState.charAt(0).toUpperCase() + serState.substr(1).toLowerCase(),
                  "short": true
                },
                {
                  "title": "List of active incidents",
                  "value": (details[1].length) ? pickIncs(incdetails) : "No active incidents",
                  "short": true
                },
                {
                  "title": "Is the Service frozen?",
                  "value": (servicedetails.isfrozen) ? 'YES' : 'NO',
                  "short": true
                },
                {
                  "title": "Cause of freeze",
                  "value": (servicedetails.isfrozen) ? freezeCause : 'Not Applicable',
                  "short": true
                },
                {
                  "title": "Associated Configuration Items",
                  "value": pickCis(servicedetails) || "Nill",
                  "short": true
                }
              ]
            }
          ]
        };
        return res.ok(output);
      }
    })
    .catch(function (error)
    {
      if(error.code === 400542)
      {
        output = 
        {
          "attachments": 
          [
            {
              "fallback": "Requested service " + req.body.text + " doesn't exist",
              "title": "Requested service " + req.body.text + " doesn't exist",
              "title_link": "https://app.queueload.com/",
              "text": "Please provide a valid service name",
              "color": "warning"
            }
          ]
        };
        return res.ok(output);
      }
      else
      {
        return res.negotiate(error);  
      }
    });
  },
  getcidetails: function(req, res)
  {
    if(!req.body.text || Array.isArray(req.body.text))
    {
      var error = new Error();
      error.code = 400051;
      error.message = errorCode[ "400051" ];
      return res.negotiate(error);
    }
    var db;
    var output = {};
    Account.findOne({ select: [ 'database' ], where: { slackteamid: req.body.team_id } })
    .then(function (result)
    {
      connection.database = result.database;
      connection.user = result.database;
      connection.password = result.database;
      db = cache.get(connection.database);
      if(!db)
      {
        db = pgp(connection);
        cache.put(connection.database, db);
      }
      var pGetCidetails = db.any(
      {
        text: "select cidetails.ciname,cidetails.assettag,cidetails.ipaddress,cidetails.cistateid,cienvironments.envname,cihardwares.hardwaretag,cilocations.locationcode,vendors.vendorname,citypes.citype,incidents.incidentid  from cidetails \
                left outer join cienvironments on cidetails.envid = cienvironments.envid \
                left outer join cihardwares on cidetails.machineid = cihardwares.machineid \
                left outer join cilocations on cidetails.locationid = cilocations.locationid \
                left outer join vendors on cidetails.vendorid = vendors.vendorid \
                left outer join citypes on cidetails.citypeid = citypes.citypeid \
                left outer join incidents on incidents.ciid = cidetails.ciid \
                where cidetails.ciname = $1 \
                and incidents.state != $2;",
        values: [req.body.text, enums.incidentStates.CLOSED]
      });

      var pGetServices = db.any(
      {
        text: "select services.servicename,cidetails.ciname from services \
                left outer join cidetail_serviceids__service_ciids \
                on service_ciids = services.serviceid \
                left outer join cidetails \
                on cidetail_serviceids = cidetails.ciid \
                where cidetails.ciname = $1;", 
        values: [req.body.text]
      });
      return Promise.all([pGetCidetails, pGetServices]);
    })
    .then(function (details)
    {
      if(!details[0].length)
      {
        var error = new Error();
        error.code = 400543;
        throw error;
      }
      else
      {
        var cidetails = details[0];
        var servicedetails = details[1];
        var enumToString = function (enums, value) 
        {
          for (var k in enums)
          {
            if (enums[k] === value) 
              return k;
          }
          return null;
        };
        var ciState = enumToString(enums.ciStateIds, cidetails[0].cistateid);
        var padding = function (number, width, paddingchar)
        {
          paddingchar = paddingchar || '0';
          number = number + '';
          return number.length >= width ? number : new Array(width - number.length + 1).join(paddingchar) + number;
        };
        var pickIncs = function(objects)
        {
          var inclist = '';
          for(var i = 0; i < objects.length; i++)
          {
            inclist = inclist + "INC" + padding(objects[i].incidentid, 5, 0) + '\n';
          }
          return inclist;
        };
        var pickServices = function(objects)
        {
          var servlist = '';
          for(var i = 0; i < objects.length; i++)
          {
            servlist = servlist + objects[i].servicename + '\n';
          }
          return servlist;   
        };
        output = 
        {
          "attachments": 
          [
            {
              "fallback": "[CI Summary] " + req.body.text,
              "color": "#663399",
              "pretext": "CI details: " + req.body.text,
              "title": cidetails[0].ciname,
              "title_link": "https://app.queueload.com/",
              "fields": 
              [
                {
                  "title": "CI Type",
                  "value": cidetails[0].citype,
                  "short": true
                },
                {
                  "title": "CI State",
                  "value": ciState.charAt(0).toUpperCase() + ciState.substr(1).toLowerCase(),
                  "short": true
                },
                {
                  "title": "IP Address",
                  "value": cidetails[0].ipaddress || "Nill",
                  "short": true
                },
                {
                  "title": "Environment",
                  "value": cidetails[0].envname,
                  "short": true
                },
                {
                  "title": "Location Code",
                  "value": cidetails[0].locationcode,
                  "short": true
                },
                {
                  "title": "Vendor",
                  "value": cidetails[0].vendorname,
                  "short": true
                },                
                {
                  "title": "List of active incidents on the CI",
                  "value": pickIncs(cidetails) || "No active incidents",
                  "short": true
                },
                {
                  "title": "Associated Services",
                  "value": pickServices(servicedetails) || "Nill",
                  "short": true
                },
                {
                  "title": "Asset Tag",
                  "value": cidetails[0].assettag,
                  "short": true
                },                {
                  "title": "Hardware Tag",
                  "value": cidetails[0].hardwaretag || "Nill",
                  "short": true
                }
              ]
            }
          ]
        };
        return res.ok(output);
      }
    })
    .catch(function (error)
    {
      if(error.code === 400543)
      {
        output = 
        {
          "attachments": 
          [
            {
              "fallback": "Requested CI " + req.body.text + " doesn't exist",
              "title": "Requested CI " + req.body.text + " doesn't exist",
              "title_link": "https://app.queueload.com/",
              "text": "Please provide a valid ci name",
              "color": "warning"
            }
          ]
        };
        return res.ok(output);
      }
      else
      {
        return res.negotiate(error);  
      }
    });
  }
};
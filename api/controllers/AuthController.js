/* FileName: AuthController.js
* @description: This file describes the various actions in the authorization controller.
*
* Date          Change Description                            Author
* ---------     ----------------------                        -------
* 02/11/2016    Initial file creation                         SMandal
*
*/

/**
 * @apiGroup AAA
 */

var request = require('request-promise'); 
var pgp = require('pg-promise')();
var cache = require('memory-cache');
var Promise = require('bluebird');
var enums = require("../resources/Enums.js");

var connection = {
  user: 'qlodtest',
  database: 'qlodtest',
  password: 'qlodtest'
};

if(sails.config.environment === 'development' || sails.config.environment === 'staging' || sails.config.environment === 'test')
{
  connection.host = sails.config.connections.localAccountsRepo.host;
  connection.port = sails.config.connections.localAccountsRepo.port;
}
else if(sails.config.environment === 'production')
{
  connection.host = sails.config.connections.awsAccountsRepo.host;
  connection.port = sails.config.connections.awsDataRepo.port;
}
else
{
  sails.log.error("No environment set!!");
}

module.exports = {
  slackauth: function(req, res)
  {
    var configurl, accountDet;
    Account.findOne({accountid: req.query.state})
    .then(function(account)
    {
      accountDet = account;
      if(!account)
      {
        var error = new Error();
        error.code = 401;
        error.message = 'Invalid accountid. Please contact your administrator';
        throw error;
      }
      else
      {
        return request('https://slack.com/api/oauth.access?client_id=' + sails.config.slackcfg.SLACK_CLIENT_ID + '&client_secret=' + sails.config.slackcfg.SLACK_CLIENT_SECRET + '&code=' + req.query.code + '&pretty=1');
      }
    })    
    .then(function (body)
    {
      /*
      sails.log.debug("Received body from Slack oauth", body);
        {
          "ok": true,
          "access_token": "xoxp-12030263152-12030263168-146068791718-d42f99d2d87bf96bd99e3be909eb0662",
          "scope": "identify,bot,commands,incoming-webhook,team:read",
          "user_id": "U0C0W7R4Y",
          "team_name": "QueueLoad",
          "team_id": "T0C0W7R4G",
          "incoming_webhook": {
              "channel": "@smrutimandal",
              "channel_id": "D1CSP75RQ",
              "configuration_url": "https:\/\/queueload.slack.com\/services\/B49DRJK9S",
              "url": "https:\/\/hooks.slack.com\/services\/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"
          },
          "bot": {
              "bot_user_id": "U39LN536F",
              "bot_access_token": "xoxb-111702173219-CUT349CWdC1dOqM6tlzlLJfv"
          }
        }
      */
      configurl = JSON.parse(body).incoming_webhook.configuration_url;
      connection.database = req.query.state;
      connection.user = req.query.state;
      connection.password = req.query.state;
      var db = cache.get(connection.database);
      if(!db)
      {
        db = pgp(connection);
        cache.put(connection.database, db);
      }
      var slackConfig = {
        slackteamid: JSON.parse(body).team_id,
        slackincomingwebhookurl: JSON.parse(body).incoming_webhook.url
      };
      var intConfigUpdate = db.any(
      {
        text: "INSERT INTO integrationconfigs (integrationtype, integratorid, config, \"createdAt\", \"updatedAt\") VALUES ($1, $2, $3, $4, $5);",
        values: [ enums.integrationService.SLACK, 1, slackConfig, Math.floor(new Date() / 1000), Math.floor(new Date() / 1000) ]
      });
      if(!accountDet.config)
      {
        accountDet.config = {};
      }
      accountDet.config.slackteamid = JSON.parse(body).team_id;
      var accountUpd = Account.update({accountid: accountDet.accountid}, {config: accountDet.config});
      return Promise.all([intConfigUpdate, accountUpd]);
    })
    .then(function (result)
    {
      return res.redirect(configurl);  
    })
    .catch(function (error)
    {
      return res.negotiate(error);
    });
  },
  slackevents: function(req, res)
  {
    if(req.body.type === 'url_verification')
    {
      return res.ok({challenge: req.body.challenge});  
    }
    else if(req.body.event.type === 'app_uninstalled')
    {
      var accountQueryAsync = Promise.promisify(Account.query);
      accountQueryAsync('select * from accounts where config->>$1=$2', ['slackteamid', req.body.team_id])
      .then(function (account)
      {
        connection.database = account.rows[0].accountid;
        connection.user = account.rows[0].accountid;
        connection.password = account.rows[0].accountid;
        var db = cache.get(connection.database);
        if(!db)
        {
          db = pgp(connection);
          cache.put(connection.database, db);
        }
        var intConfigUpdate = db.any(
        {
          text: "DELETE FROM integrationconfigs WHERE integrationtype=$1;",
          values: [ enums.integrationService.SLACK ]
        });
        delete account.rows[0].config.slackteamid;
        var accountUpd = Account.update({accountid: account.rows[0].accountid}, {config: account.rows[0].config});
        return Promise.all([intConfigUpdate, accountUpd]);
      })
      .then(function (result)
      {
        return res.ok();
      })
      .catch(function (error)
      {
        return res.negotiate(error);
      });
    }
    else
    {
      return res.ok();  
    }
  }
};